from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from builders import views


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('builders.views',
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/builders/', 'builders_list'),
    url(r'^api/trades/', 'trade_types'),
    url(r'^api/app_users/', 'app_users'),

)

urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, }),
    )
