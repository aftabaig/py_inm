from rest_framework import serializers

# models
from models import Builder
from models import Trade
from models import AppUser


class BuilderSerializer(serializers.ModelSerializer):
	class Meta:
		model = Builder

class TradeSerializer(serializers.ModelSerializer):
	class Meta:
		model = Trade

class AppUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = AppUser
			