from django.contrib import admin
from django.contrib.sites.models import *
from django.contrib.auth.models import *
from builders.models import *
import urllib
import json

class BuilderAdmin(admin.ModelAdmin):

	list_display = ['name','address','postalCode']
	exclude = ['dateUpdated','latitude','longitude']

	def save_model(self, request, object, form, change):
		str_lat_lon = self.geocode(object.address)
		if str_lat_lon is not None:
			arr_lat_lon = str_lat_lon.split(',')
			object.latitude = arr_lat_lon[0]
			object.longitude = arr_lat_lon[1]
			object.save()
		else:
			object.save()

	def geocode(self, location):
		output = "csv"
		location = urllib.quote_plus(location)
		request = "http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=true" % (location)
		data = urllib.urlopen(request).read()
		jsonData = json.loads(data)
		results = jsonData['results']
		if len(results) > 0:
			result = results[0]
			geometry = result['geometry']
			location = geometry['location']
			lat = location['lat']
			lng = location['lng']
			return "%s,%s" % (lat, lng)
		else:
			return None

class TradeAdmin(admin.ModelAdmin):
	pass

class AppUserAdmin(admin.ModelAdmin):
	pass

admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.unregister(Site)

admin.site.register(Builder, BuilderAdmin)
admin.site.register(Trade, TradeAdmin)
admin.site.register(AppUser, AppUserAdmin)