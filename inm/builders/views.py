from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

# serializers
from serializers import BuilderSerializer
from serializers import TradeSerializer
from serializers import AppUserSerializer

# models
from models import Builder
from models import Trade
from models import AppUser

@api_view(['GET'])
def builders_list(request):

	latitude = request.QUERY_PARAMS.get('latitude', None)
	longitude = request.QUERY_PARAMS.get('longitude', None)

	if latitude is not None:

		builders = Builder.objects.raw('SELECT *, (6371 * acos(cos(radians(%s)) * cos(radians(latitude)) * cos(radians(longitude) - radians(%s)) + sin(radians(%s)) * sin(radians(latitude)))) AS distance FROM builders_builder ORDER BY distance ASC', [latitude, longitude, latitude])
		
		# deserializing to include "distance" in the response.
		results = list()
		for builder in builders:
			serializer = BuilderSerializer(builder);
			name = serializer.data['name']
			telephone = serializer.data['telephone']
			fax = serializer.data['fax']
			address = serializer.data['address']
			postalCode = serializer.data['postalCode']
			email = serializer.data['email']
			logo = serializer.data['logo']
			openHoursWeekdays = serializer.data['openHoursWeekdays']
			openHoursSaturdays = serializer.data['openHoursSaturdays']
			latitude = serializer.data['latitude']
			longitude = serializer.data['longitude']
			dateUpdated = serializer.data['dateUpdated']
			distance = builder.distance
			dictionary = {'name': name, 'telephone': telephone, 'fax': fax, 'address': address, 'postalCode': postalCode, 'email': email, 'logo': logo, 'openHoursWeekdays': openHoursWeekdays, 'openHoursSaturdays': openHoursSaturdays, 'latitude': latitude, 'longitude': longitude, 'dateUpdated': dateUpdated, 'distance': distance}
			results.append(dictionary)
	
	return Response(results)

@api_view(['GET'])
def trade_types(request):

	trades = Trade.objects.all()
	serializer = TradeSerializer(trades)
	return Response(serializer.data)

@api_view(['GET', 'POST'])
def app_users(request):

	if request.method == 'GET':
		users = AppUser.objects.all()
		serializer = AppUserSerializer(users)
		return Response(serializer.data)
	elif request.method == 'POST':
		serializer = AppUserSerializer(data=request.DATA)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




