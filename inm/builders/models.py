from django.db import models
import datetime

class Builder(models.Model):
	name = models.CharField('Name', max_length=64)
	telephone = models.CharField('Telephone', max_length=32)
	fax = models.CharField('Fax', max_length=32)
	address = models.CharField('Address', max_length=1024)
	postalCode = models.CharField('Post Code', max_length=16)
	email = models.CharField('Email Address', max_length=32)
	logo = models.ImageField('Logo', upload_to='builders/', blank=True, max_length=64)
	openHoursWeekdays = models.CharField('Opening Hours (Weekdays)', max_length=16)
	openHoursSaturdays = models.CharField('Opening Hours (Saturdays)', max_length=16)
	latitude = models.FloatField(default=0.0)
	longitude = models.FloatField(default=0.0)
	dateUpdated = models.DateTimeField(default=datetime.datetime.now())
	
class Trade(models.Model):
	name = models.CharField('Name', max_length=64)

class AppUser(models.Model):
	name = models.CharField('Name', max_length=64)
	email = models.EmailField('Email Address', max_length=32)
	companyName = models.CharField('Company Name', max_length=64)
	isMember = models.BooleanField('Are you a member of www.ineedanexpert.co.uk ?')
	tradeType = models.ForeignKey('Trade')